import unittest
from Player import *
from AggressivePlayer import *
from Card import Card
from Token import Token
from Parameters import *
import copy

class PlayerTest(unittest.TestCase):
    def test_playerNameIsKalle(self):
        player = Player()
        self.assertEqual('Kalle', player.getName())

    def test_playerPointsIsZero(self):
        player = Player()
        self.assertEqual(0, player.getPoints())

    def test_playerAddBonusPointsSucceeds(self):
        player = Player()
        player.addBonusPoints(10)
        self.assertEqual(10, player.getPoints())

    def test_playerScoreOneCard(self):
        player = Player()
        card = Card(points=7)
        player.scoreOneCard(card)
        self.assertEqual(7, player.calculateTotalPoints())

    def test_playerScoresOneToken(self):
        player = Player()
        token = Token()
        player.scoreOneToken(token)
        self.assertEqual(TOKEN_VALUE, player.calculateTotalPoints())

    def test_playerScoresTwoTokensButLosesOne(self):
        player = Player()
        token = Token()
        player.scoreOneToken(token)
        player.scoreOneToken(token)
        player.removeOneToken()
        self.assertEqual(TOKEN_VALUE, player.calculateTotalPoints())

    def test_playerPlaysOneCardAndScores(self):
        player = Player()
        card = Card(points=7)
        player.playCard(card)
        self.assertEqual(7, player.calculateTotalPoints())

#    def test_cardWithDamage3CanBeTackledWith3Damage(self):
#        player = Player()
#        card = Card(vahinko=3)
#        dice = [DieFaces.damage.value, DieFaces.damage.value, DieFaces.damage.value]
#        tokenDice = [UNDEF, UNDEF]
#        self.assertTrue(player.cardCanBeTackledWithTokenDice(card, dice, tokenDice))

    def test_rollTokenFor3Damage1Range(self):
        player = Player()
        player.scoreOneToken(Token())
        card = Card(damage=3, range=1)
        dice = [DieFaces.damage.value, DieFaces.damage.value, DieFaces.damage.value]
        tokenDice = [UNDEF, UNDEF]
        expectedDice = [UNDEF, REROLL]
        self.assertEqual(expectedDice, player.analyzeFurtherThrows(card, dice, tokenDice))
    def test_rollTokenFor1Damage3Range(self):
        player = Player()
        player.scoreOneToken(Token())
        card = Card(damage=1, range=3)
        dice = [DieFaces.range.value, DieFaces.range.value, DieFaces.range.value]
        tokenDice = [UNDEF, UNDEF]
        expectedDice = [UNDEF, REROLL]
        self.assertEqual(expectedDice, player.analyzeFurtherThrows(card, dice, tokenDice))
    def test_rollTokenFor2Damage3Range(self):
        player = Player()
        player.scoreOneToken(Token())
        card = Card(damage=2, range=3)
        dice = [DieFaces.range.value, DieFaces.range.value, DieFaces.range.value]
        tokenDice = [DieFaces.damage.value, UNDEF]
        expectedDice = [DieFaces.damage.value, REROLL]
        self.assertEqual(expectedDice, player.analyzeFurtherThrows(card, dice, tokenDice))
    def test_rollOneBranchAwayWithTokens(self):
        player = Player()
        player.scoreOneToken(Token())
        card = Card(damage=1, range=3)
        dice = [DieFaces.range.value, DieFaces.range.value, DieFaces.range.value]
        tokenDice = [DieFaces.damage.value, DieFaces.branch.value]
        expectedDice = [DieFaces.damage.value, REROLL]
        self.assertEqual(expectedDice, player.analyzeFurtherThrows(card, dice, tokenDice))
    def test_notEnoughTokensToRollBranchAway(self):
        player = Player()
        player.scoreOneToken(Token())
        card = Card(damage=1, range=3)
        dice = [DieFaces.range.value, DieFaces.range.value, DieFaces.range.value]
        tokenDice = [DieFaces.branch.value, DieFaces.damage.value]
        expectedDice = [DieFaces.branch.value, DieFaces.damage.value]
        self.assertEqual(expectedDice, player.analyzeFurtherThrows(card, dice, tokenDice))
    def test_rerollJaegerToRemoveOneBranch(self):
        player = Player()
        player.scoreOneToken(Token())
        player.scoreOneToken(Token())
        card = Card(damage=3, range=1)
        dice = [DieFaces.damage.value, DieFaces.damage.value, DieFaces.damage.value]
        tokenDice = [DieFaces.branch.value, DieFaces.jaeger.value]
        expectedDice = [DieFaces.branch.value, REROLL]
        self.assertEqual(expectedDice, player.analyzeFurtherThrows(card, dice, tokenDice))

    def test_noRerollsForZeroDamageAndRange(self):
        player = Player()
        card = Card(name='Karhu', damage=0, range=0)
        dice = [DieFaces.damage.value, DieFaces.range.value, DieFaces.range.value]
        analyzedDice = player.analyzeFirstThrow(card, dice)
        self.assertEqual(dice, analyzedDice)

    def test_diceAreNotRerolledWhenCardWouldBeTackled(self):
        player = Player()
        card = Card(name='Karhu', damage=1, range=0)
        dice = [DieFaces.damage.value, DieFaces.range.value, DieFaces.range.value]
        analyzedDice = player.analyzeFirstThrow(card, dice)
        self.assertEqual(0, analyzedDice.count(REROLL))

    def test_oneBranchIsRerolled(self):
        player = Player()
        card = Card(name='Karhu', damage=1, range=0)
        dice = [DieFaces.branch.value, DieFaces.damage.value, DieFaces.range.value]
        analyzedDice = player.analyzeFirstThrow(card, dice)
        refAnalyzedDice = copy.deepcopy(dice)
        refAnalyzedDice[0] = REROLL
        self.assertEqual(refAnalyzedDice, analyzedDice)

    def test_twoBranchesAreRerolled(self):
        player = Player()
        card = Card(name='Karhu', damage=0, range=1)
        dice = [DieFaces.branch.value, DieFaces.branch.value, DieFaces.range.value]
        analyzedDice = player.analyzeFirstThrow(card, dice)
        refAnalyzedDice = copy.deepcopy(dice)
        refAnalyzedDice[0] = REROLL
        refAnalyzedDice[1] = REROLL
        self.assertEqual(refAnalyzedDice, analyzedDice)

    def test_oneDamageIsRerolledToRange(self):
        player = Player()
        card = Card(name='Karhu', damage=1, range=2)
        dice = [DieFaces.damage.value, DieFaces.damage.value, DieFaces.range.value]
        analyzedDice = player.analyzeFirstThrow(card, dice)
        refAnalyzedDice = [DieFaces.damage.value, REROLL, DieFaces.range.value]
        self.assertEqual(refAnalyzedDice, analyzedDice)

    def test_oneRangeIsRerolledToDamage(self):
        player = Player()
        card = Card(name='Karhu', damage=2, range=1)
        dice = [DieFaces.range.value, DieFaces.damage.value, DieFaces.range.value]
        analyzedDice = player.analyzeFirstThrow(card, dice)
        refAnalyzedDice = [DieFaces.range.value, DieFaces.damage.value, REROLL]
        self.assertEqual(refAnalyzedDice, analyzedDice)

    def test_notEnoughTokensToTackleCardSoRerollForJaeger(self):
        player = Player()
        card = Card(name='Karhu', damage=4, range=1)
        dice = [DieFaces.damage.value, DieFaces.damage.value, DieFaces.jaeger.value]
        analyzedDice = player.analyzeFirstThrow(card, dice)
        expectedDice = [REROLL, REROLL, DieFaces.jaeger.value]
        self.assertEqual(expectedDice, analyzedDice)

    def test_rolling3DamageScoresCardWith1Damage(self):
        animalCard = Card(name='Karhu', damage=1, range=0)
        dieResult = [DieFaces.damage.value, \
                     DieFaces.damage.value, \
                     DieFaces.damage.value]
        self.assertTrue(cardIsTackled(animalCard, dieResult))

    def test_rolling1Damage1RangeScoresCardWith1Damage1Range(self):
        animalCard = Card(name='Karhu', damage=1, range=1, points=7)
        dieResult = [DieFaces.damage.value, \
                     DieFaces.range.value, \
                     DieFaces.damage.value]
        self.assertTrue(cardIsTackled(animalCard, dieResult))

    def test_rolling1Damage1Range1JaegerScoresCardWith1Damage2Range(self):
        animalCard = Card(name='Karhu', damage=1, range=2, points=7)
        dieResult = [DieFaces.damage.value, \
                     DieFaces.range.value, \
                     DieFaces.jaeger.value]
        self.assertTrue(cardIsTackled(animalCard, dieResult))

    def test_rolling1JaegerScoresCardWith1Damage(self):
        animalCard = Card(name='Karhu', damage=1, range=0, points=7)
        dieResult = [DieFaces.range.value, \
                     DieFaces.range.value, \
                     DieFaces.jaeger.value]
        self.assertTrue(cardIsTackled(animalCard, dieResult))

    def test_rolling1BranchFails(self):
        animalCard = Card(name='Karhu', damage=1, range=0, points=7)
        dieResult = [DieFaces.range.value, \
                     DieFaces.range.value, \
                     DieFaces.branch.value]
        self.assertFalse(cardIsTackled(animalCard, dieResult))

    def test_firstThrowBranchesAreNotRerolledWithTokens(self):
        player = Player()
        player.scoreOneToken(Token())
        card = Card('Karhu', damage=2, range=1, points=7)
        dice = [DieFaces.branch.value, \
                DieFaces.damage.value, \
                DieFaces.damage.value]
        expectedDice = [UNDEF, UNDEF]
        tokenDice = [UNDEF]*MAX_BONUS_DICE
        dieResult = player.analyzeFurtherThrows(card, dice, tokenDice)
        self.assertEqual(expectedDice, dieResult)

    def test_noTokenDiceAreThrownWithoutTokens(self):
        player = Player()
        card = Card('Karhu', damage=2, range=2, points=7)
        dice = [DieFaces.range.value, \
                DieFaces.damage.value, \
                DieFaces.damage.value]
        expectedDice = [UNDEF, UNDEF]
        tokenDice = [UNDEF]*MAX_BONUS_DICE
        dieResult = player.analyzeFurtherThrows(card, dice, tokenDice)
        self.assertEqual(expectedDice, dieResult)

    def test_throwOneTokenDieWithOneToken(self):
        player = Player()
        player.scoreOneToken(Token())
        card = Card('Karhu', damage=2, range=2, points=7)
        dice = [DieFaces.range.value, \
                DieFaces.damage.value, \
                DieFaces.damage.value]
        tokenDice = [UNDEF]*MAX_BONUS_DICE
        dieResult = player.analyzeFurtherThrows(card, dice, tokenDice)
        self.assertEqual(REROLL, dieResult[-1])

    def test_utilizeBothRethrowsWithTokens(self):
        player = Player()
        player.scoreOneToken(Token())
        player.scoreOneToken(Token())
        card = Card('Karhu', damage=4, range=1, points=17)
        dice = [DieFaces.range.value, \
                DieFaces.jaeger.value, \
                DieFaces.damage.value]
        tokenDice = [UNDEF]*MAX_BONUS_DICE
        tokenDice = player.analyzeFurtherThrows(card, dice, tokenDice)
        tokenDice = tokenDice[::-1]
        tokenDice[0] = DieFaces.damage.value
        dieResult = player.analyzeFurtherThrows(card, dice, tokenDice)
        self.assertEqual(REROLL, dieResult[-1])

    def test_dontWasteTokensIfCardCannotBeTackled(self):
        player = Player()
        player.scoreOneToken(Token())
        card = Card('Karhu', damage=2, range=2, points=15)
        dice = [DieFaces.range.value,\
                DieFaces.range.value,\
                DieFaces.range.value]
        tokenDice = [UNDEF]*MAX_BONUS_DICE
        dieResult = player.analyzeFurtherThrows(card, dice, tokenDice)
        self.assertEqual([UNDEF, UNDEF], dieResult)

    def test_rerollBranchAndDamage(self):
        player = Player()
        player.scoreOneToken(Token())
        card = Card('Karhu', damage=0, range=3, points=15)
        dice = [DieFaces.branch.value,\
                DieFaces.damage.value,\
                DieFaces.jaeger.value]
        dieResult = player.analyzeFirstThrow(card, dice)
        expectedDice = [REROLL, REROLL, DieFaces.jaeger.value]
        self.assertEqual(expectedDice, dieResult)

    def test_safePlayerWillNotThrowExtraDiceToJaeger(self):
        player = Player()
        card = Card('Ilves', damage=1, range=1, points=5)
        dice = [DieFaces.range.value,\
                DieFaces.damage.value,\
                DieFaces.range.value]
        dieResult = player.analyzeFirstThrow(card, dice)
        expectedDice = [DieFaces.range.value,\
                        DieFaces.damage.value,\
                        DieFaces.range.value]
        self.assertEqual(expectedDice, dieResult)

    def test_aggressivePlayerWillThrowExtraDiceToJaeger(self):
        player = AggressivePlayer()
        card = Card('Ilves', damage=1, range=1, points=5)
        dice = [DieFaces.range.value,\
                DieFaces.damage.value,\
                DieFaces.range.value]
        dieResult = player.analyzeFirstThrow(card, dice)
        expectedDice = [DieFaces.range.value,\
                        DieFaces.damage.value,\
                        REROLL]
        self.assertEqual(expectedDice, dieResult)


if __name__ == '__main__':
    unittest.main()