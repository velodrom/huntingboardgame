import unittest
from Card import Card
from Deck import Deck

class DeckTest(unittest.TestCase):
    def test_deckSizeZero(self):
        jaegerDeck = Deck()
        deckSize = jaegerDeck.numCardsInDeck()
        self.assertEqual(0, deckSize)
    
    def test_deckWithOneCard(self):
        jaegerDeck = Deck()
        animal = Card()
        jaegerDeck.addCard(animal)
        self.assertEqual(1,jaegerDeck.numCardsInDeck())

    def test_correctCardIsPoppedFromDeckOfSizeOne(self):
        jaegerDeck = Deck()
        animal = Card('Karhu')
        jaegerDeck.addCard(animal)
        self.assertEqual('Karhu', jaegerDeck.popCard().getName())

    def test_lastCardIsPoppedFromDeckOfSizeTwo(self):
        jaegerDeck = Deck()
        animal1 = Card('Karhu')
        animal2 = Card('Fasaani')
        jaegerDeck.addCard(animal1)
        jaegerDeck.addCard(animal2)
        self.assertEqual('Fasaani', jaegerDeck.popCard().getName())

    def test_popTooManyCardFromDeckFails(self):
        jaegerDeck = Deck()
        animal = Card()
        jaegerDeck.addCard(animal)
        jaegerDeck.popCard()
        jaegerDeck.popCard()
        self.assertEqual(None, jaegerDeck.popCard())

if __name__ == '__main__':
    unittest.main()