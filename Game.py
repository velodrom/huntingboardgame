import collections
from Deck import Deck
from Card import *
from Player import *
from AggressivePlayer import *
from Parameters import *
import random
import csv
import math
import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerLine2D
import numpy as np

class GameController():
    def __init__(self, logFileName='GameLog.csv'):
        self.logger = openLogger(logFileName)
        self.playerPool = ['Kalle', 'Mikko', 'Petra', 'Teija']

    def __del__(self):
        del(self.logger)

    def simulateGames(self, numGames=10, numPlayers=2):
        players = random.sample(self.playerPool, numPlayers)
        headerRow = 'Game,'+','.join(players)
        self.logger.writerow(headerRow)
        gameScores = [[] for i in range(numPlayers)]
        games = []
        for gameNumber in range(numGames):
            jGame = Game(deck=Deck(defaultDeck=True), logger=self.logger)
            for player in players:
                jGame.addPlayer(player)

            jGame.playCompleteGame()

            newRow = str(gameNumber+1)+','
            for playerIdx, player in enumerate(jGame.players):
                newRow += str(player.calculateTotalPoints())+','
                gameScores[playerIdx].append(copy.deepcopy(player))
            self.logger.writerow(newRow)
            games.append(jGame)
        self.plotResults(gameScores, games)

    def plotResults(self, gameScores, gameList):
        numPlayers = len(gameScores)
        concatScoreObjs = [obj for sublist in gameScores for obj in sublist]
        gameScores1D = sum(list(map(lambda player: player.calculateTotalPoints(), concatScoreObjs)), [])
        plt.figure(1)
        plt.suptitle('Simulations: {:d}, Players: {:d}'.format(\
            len(gameScores[0]), numPlayers))
        plt.subplot(221)
        binwidth=2
        scoreMean = sum(gameScores1D)/len(gameScores1D)
        scoreVar  = sum(map(lambda score: (score-scoreMean)**2, gameScores1D))/len(gameScores1D)
        plotBins = np.arange(0, max(gameScores1D)+binwidth, binwidth)
        count, bins, ignored = plt.hist(gameScores1D, bins=plotBins, normed=1,\
                                        label=[r'$\mu={:.2f},\ \sigma^2={:.2f}$'.format(\
                                            scoreMean, scoreVar)])

        plt.plot(bins, 1/(np.sqrt(2 * np.pi * scoreVar)) * \
                 np.exp(-(bins-scoreMean)**2 / (2 * scoreVar) ), \
                 linewidth=2, color='k')
        plt.legend(prop={'size':10})
        xmin, xmax, ymin, ymax = plt.axis()
        plt.axis([0, max(160, xmax), ymin, ymax])
        plt.ylabel('Frequency')
        plt.xlabel('Overall score distribution')
        #plt.show()

        plt.subplot(222)
        scoreArray = np.array(gameScores)
        for column in range(len(gameScores[0])):
            scoreArray[:, column] = sorted(scoreArray[:,column], reverse=True)
        scorePerPosition = {}
        for playerIdx in range(0,numPlayers):
            posScore = scoreArray[playerIdx,:]
            scoreMean = sum(posScore)/len(posScore)
            scoreVar  = sum(map(lambda score: (score-scoreMean)**2, \
                                posScore))/len(posScore)
            scorePerPosition[playerIdx] = {'mean': scoreMean, 'var': scoreVar}
            count, bins, ignored = plt.hist(posScore, bins=plotBins, normed=1,\
                                            label=[r'$\mu={:.2f},\ \sigma^2={:.2f}$'.format(\
                                                scoreMean, scoreVar)])
            plt.plot(bins, 1/(np.sqrt(2 * np.pi * scoreVar)) * \
                        np.exp(-(bins-scoreMean)**2 / (2 * scoreVar) ), \
                        linewidth=2, color='k')
            plt.legend(prop={'size':10})
        xmin, xmax, ymin, ymax = plt.axis()
        plt.axis([0, max(160, xmax), ymin, ymax])
        plt.ylabel('Frequency')
        plt.xlabel('Ranking score pdf')

        plt.subplot(223)
        winnerScore = scoreArray[0,:]
        for playerIdx in range(1,numPlayers):
            diffScore = winnerScore-scoreArray[playerIdx,:]
            diffMean = sum(diffScore)/len(diffScore)
            diffVar  = sum(map(lambda score: (score-diffMean)**2, \
                                diffScore))/len(diffScore)
            count, binsH, ignored = plt.hist(diffScore, bins=plotBins, normed=1,\
                                            label=[r'$\mu={:.2f},\ \sigma^2={:.2f}$'.format(\
                                                diffMean, diffVar)])
            bins = range(0,160,1)
            if POISSON_MODEL and playerIdx == 1:
                lmd = diffMean
                poissonPdf = list(map(lambda k: (lmd**k*math.e**(-lmd))/(math.factorial(k)), bins))
                #plt.plot(bins, poissonPdf, \
                #         linewidth=2, color='k')
                fio = open('data.txt','w')
                fio.write("data = [ ...\n")
                for item in diffScore:
                    fio.write("%s; ...\n" % item)
                fio.write("];\n")
                self.writeMatlabPlotScript(fio)
                fio.close()
            else:
                plt.plot(bins, 1/(np.sqrt(2 * np.pi * diffVar)) * \
                            np.exp(-(bins-diffMean)**2 / (2 * diffVar) ), \
                            linewidth=2, color='k')
            plt.legend(prop={'size':10})
        plt.xlabel('Difference to winner score')
        plt.show()

        # Plot scores per player profile
        plt.figure(2)


    def writeMatlabPlotScript(self, fileDescriptor):
        script = "data = data+1;\n"+\
        "paramEst = wblfit(data);\n"+\
        "n = length(data);\n"+\
        "binWidth = 2;\n"+\
        "binCtrs = 1:binWidth:60; counts = hist(data,binCtrs); prob = counts / (n*binWidth);\n"+\
        "bar(binCtrs, prob, 'hist');\n"+\
        "xgrid = linspace(0,20,100);\n"+\
        "xgrid = linspace(0,60,100);\n"+\
        "pdfEst = wblpdf(xgrid, paramEst(1), paramEst(2));\n"+\
        "line(xgrid, pdfEst)\n"
        fileDescriptor.write("%s\n" % script)

class Game():
    def __init__(self, numCards=20, deck=None, logger=None):
        self.deck = deck if deck is not None else Deck()
        self.replayDeck = Deck()
        self.discardDeck = Deck()
        self.players = []
        self.numPlayedTurns = 0
        if self.deckSize() == 0:
            self.initializeRandomDeck(numCards)
        self.logger = logger if logger is not None else openLogger('GameLog.csv')
        if VERBOSITY > 1:
            self.logger.writerow("Turn;Layer;CardName;Damage;Range;Points;Tokens;"+\
                                 "FirstThrow;"+';'*(MAX_DEFAULT_DICE-1)+\
                                 "AnalyzedDice;"+';'*(MAX_DEFAULT_DICE-1)+\
                                 "RerolledDice;"+';'*(MAX_DEFAULT_DICE-1)+\
                                 "BonusDice;"+';'*(MAX_BONUS_DICE-1)+"CardScored")

    def __del__(self):
        del(self.logger)

    def deckSize(self):
        return self.deck.numCardsInDeck()

    def addRandomCard(self):
        rndCard = self.getRandomCard()
        self.deck.addCard(rndCard)

    def getRandomCard(self):
        animal = random.choice(allNames)
        vahinko = min(random.choice(allDamage), MAX_GAME_DICE)
        range   = min(random.choice(allRange), MAX_GAME_DICE)
        if vahinko+range > MAX_GAME_DICE:
            range = max(MAX_GAME_DICE-vahinko, 0)
        pisteet = random.choice(allPoints)

        newCard = Card(animal, vahinko, range, pisteet)
        return newCard
    
    def initializeRandomDeck(self, deckSize):
        for cardNum in range(deckSize):
            self.addRandomCard()

    def getNumPlayers(self):
        return len(self.players)

    def addPlayers(self, players):
        for player in players:
            if len(self.players) < MAX_PLAYERS:
                self.players.append(player)
            else:
                break

    def addPlayer(self, playerName='Kalle'):
        if playerName == 'Kalle':
            player = AggressivePlayer(name=playerName)
        else:
            player = Player(name=playerName)
        if len(self.players) < MAX_PLAYERS:
            self.players.append(player)

    def getNextPlayerInTurn(self):
        if len(self.players) > 0:
            numPlayers = len(self.players)
            currentPlayer = self.numPlayedTurns % numPlayers
            return self.players[currentPlayer]
        else:
            return None

    def playCompleteGame(self):
        if VERBOSITY > 1:
            self.logger.writerow('Playing deck for first time')
        while self.deck.numCardsInDeck():
            playedCard = self.playOneTurn(self.deck.popCard())
            if playedCard != None:
                self.replayDeck.addCard(playedCard)
        if VERBOSITY > 1:
            self.logger.writerow('Replaying leftover deck')
        while self.replayDeck.numCardsInDeck():
            playedCard = self.playOneTurn(self.replayDeck.popCard())
            if playedCard != None:
                self.discardDeck.addCard(playedCard)

    def playOneTurn(self, revealedCard):
        self.numPlayedTurns = self.numPlayedTurns+1
        player = self.getNextPlayerInTurn()
        playedCard, finalDice = self.playCard(revealedCard, player)
        return playedCard

    def playCard(self, card, player):
        logRow = str(self.numPlayedTurns)+';'+player.getName()+';'+ \
                 card.getName()+';'+str(card.getDamage())+';'+ \
                 str(card.getRange())+';'+str(card.getPoints())+';'+ \
                 str(player.getNumTokens())+';'
        returnCard = card # If card is not scored, return it
        dice = self.throwFirstDice()
        logRow += ';'.join(map(str,dice))+';'
        firstDice = player.analyzeFirstThrow(card, dice)
        logRow += ';'.join(map(str,firstDice))+';' # Log which dice are rerolled
        for dieIdx, die in enumerate(firstDice):
            if die == REROLL:
                firstDice[dieIdx] = self.throwOneDie()
        fixedDice = firstDice
        logRow += ';'.join(map(str,fixedDice))+';'

        bonusDice = [UNDEF]*MAX_BONUS_DICE # Latest throw is first in list
        while player.getNumTokens():
            analyzedDice = player.analyzeFurtherThrows(card, fixedDice, bonusDice)
            if analyzedDice[-1] == REROLL:
                player.removeOneToken()
                bonusDice[-1] = self.throwOneDie()
                bonusDice = self.rotateDice(bonusDice)
            else:
                break
        finalDice = fixedDice+bonusDice
        logRow += ';'.join(map(str,bonusDice))+';'

        if cardIsTackled(card, finalDice):
            player.scoreOneCard(card)
            returnCard = None # Card was scored
            if KILL_FOR_TOKENS:
                player.scoreTokens(finalDice)
        if not KILL_FOR_TOKENS:
            player.scoreTokens(finalDice)

        if self.threeJaegersRolled(finalDice[0:MAX_DEFAULT_DICE:1]):
            player.addBonusPoints(TRIPLEJAEGERBONUS)

        if VERBOSITY > 1:
            self.logger.writerow(logRow+ \
                                 str(returnCard == None)+';')
        return returnCard, finalDice

    def throwOneDie(self):
        return random.choice(DIE)

    def throwFirstDice(self):
        dice = []
        for dieCount in range(MAX_DEFAULT_DICE):
            dice.append(self.throwOneDie())
        return dice

    def threeJaegersRolled(self, firstDice):
        threeJaegersBonus = False
        if firstDice.count(DieFaces.jaeger.value) == MAX_DEFAULT_DICE:
            threeJaegersBonus = True
        return threeJaegersBonus

    def rotateDice(self, bonusDice):
        rotateBonusDice = collections.deque(bonusDice)
        rotateBonusDice.rotate(1)
        bonusDice = list(rotateBonusDice)
        return bonusDice

def openLogger(fileName):
    return csv.writer(open(fileName, 'w', newline=''), delimiter=' ')