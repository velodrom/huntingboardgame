from enum import Enum

TOKEN_VALUE       = 4 # 2 seems to be good
MAX_PLAYERS       = 4
MAX_DEFAULT_DICE  = 3
MAX_BONUS_DICE    = 2
MAX_GAME_DICE     = MAX_DEFAULT_DICE + MAX_BONUS_DICE
REROLL            = 0
UNDEF             = -1
TRIPLEJAEGERBONUS = 0 #5 seems to be good
KILL_FOR_TOKENS   = False # Kill is needed to score token dice

VERBOSITY         = 2
POISSON_MODEL     = False

class DieFaces(Enum):
    damage = 1
    range  = 2
    jaeger = 3
    branch = 4

DIE = [DieFaces.damage.value, \
       DieFaces.damage.value, \
       DieFaces.range.value, \
       DieFaces.range.value, \
       DieFaces.branch.value, \
       DieFaces.jaeger.value]
       
