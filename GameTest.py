import unittest
from Game import *
from Card import Card
from Deck import Deck
from Player import *
from Parameters import *

class GameTest(unittest.TestCase):
    def test_numberOfCardIsZero(self):
        jGame = Game(0)
        self.assertEqual(0, jGame.deckSize())
    
    def test_addOneCardToGame(self):
        jGame = Game(0)
        jGame.addRandomCard()
        self.assertEqual(1, jGame.deckSize())

    def test_addTwoCardsToGame(self):
        jGame = Game(0)
        jGame.addRandomCard()
        jGame.addRandomCard()
        self.assertEqual(2, jGame.deckSize())

    def test_setNumberOfCardsTo20(self):
        jGame = Game(0)
        deckSize = 20
        jGame.initializeRandomDeck(deckSize)
        self.assertEqual(deckSize, jGame.deckSize())

    def test_zeroPlayersInGame(self):
        jGame = Game()
        self.assertEqual(0, jGame.getNumPlayers())

    def test_addPlayersSucceeds(self):
        jGame = Game()
        jGame.addPlayer()
        self.assertEqual(1, jGame.getNumPlayers())

    def test_addTowPlayers(self):
        jGame = Game()
        jGame.addPlayer()
        jGame.addPlayer()
        self.assertEqual(2, jGame.getNumPlayers())

    def test_maxPlayersIsFour(self):
        jGame = Game()
        jGame.addPlayer()
        jGame.addPlayer()
        jGame.addPlayer()
        jGame.addPlayer()
        jGame.addPlayer() # Will not be added due to MAX PLAYER LIMITATION
        self.assertEqual(4, jGame.getNumPlayers())

    def test_getNextPlayerInTurn(self):
        jGame = Game()
        jGame.addPlayer('Kalle')
        jGame.addPlayer('Mikko')
        self.assertEqual('Kalle', jGame.getNextPlayerInTurn().getName())

    def test_getNextPlayerInTurnAfterOneTurn(self):
        jGame = Game()
        jGame.addPlayer('Kalle')
        jGame.addPlayer('Mikko')
        jGame.playOneTurn(Card('Myyra', damage=0,range=0, points=7))
        self.assertEqual('Mikko', jGame.getNextPlayerInTurn().getName())

    def test_getNextPlayerInTurnAfterTwoTurns(self):
        jGame = Game()
        jGame.addPlayer('Kalle')
        jGame.addPlayer('Mikko')
        jGame.playOneTurn(Card('Myyra', damage=0,range=0, points=7))
        jGame.playOneTurn(Card('Myyra', damage=0,range=0, points=7))
        self.assertEqual('Kalle', jGame.getNextPlayerInTurn().getName())

    def test_gameEndWhenTwoTurnsArePlayed(self):
        card = Card('Karhu', damage=0, range=0, points=15)
        deck = Deck([card, card])
        jGame = Game(deck=deck)
        jGame.addPlayer()
        jGame.addPlayer()
        jGame.playCompleteGame()
        self.assertEqual(2, jGame.numPlayedTurns)

    def test_playerPlaysOneTurnAndScores(self):
        card = Card('Karhu', damage=0, range=0, points=15)
        jGame = Game()
        jGame.addPlayer()
        jGame.playOneTurn(card)
        playerTotalPoints = jGame.getNextPlayerInTurn().calculateTotalPoints()
        self.assertTrue(playerTotalPoints != 0)

    def test_throwOneDie(self):
        jGame = Game(1)
        dieResult = jGame.throwOneDie()
        self.assertTrue(dieResult >= 1 or dieResult <= 6)

    def test_throwFirstDice(self):
        jGame = Game(1)
        dieResult = jGame.throwFirstDice()
        self.assertEqual(MAX_DEFAULT_DICE, len(dieResult))

    def test_oneCardIsPlayedAndScored(self):
        card = Card('Karhu', damage=0, range=0, points=15)
        deck = Deck()
        player = Player()
        deck.addCard(card)
        jGame = Game(deck=deck)
        jGame.playCard(card, player)
        self.assertEqual(15, player.calculateTotalPoints())

    def test_cardWithDamage4IsNeverScoredWithoutTokens(self):
        card = Card('Karhu', damage=4, range=0, points=15)
        deck = Deck([card]*100)
        player = Player()
        deck.addCard(card)
        jGame = Game(deck=deck)
        jGame.addPlayers([player])
        jGame.playCompleteGame()
        self.assertEqual(0, player.calculateTotalPoints())

    def test_play20CardsWith2PlayersAndVerifyTotalPoints(self):
        jGame = Game(numCards=20)
        player1 = Player('Ville')
        player2 = Player('Simo')
        jGame.addPlayers([player1, player2])
        jGame.playCompleteGame()
        self.assertTrue(player1.calculateTotalPoints() != 0)
        self.assertTrue(player2.calculateTotalPoints() != 0)


    def test_gameControllerPlays10Games(self):
        jCntrl = GameController()
        jCntrl.simulateGames(numGames=1000, numPlayers=4)

if __name__ == '__main__':
    #unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(GameTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
