from Parameters import *
from Token import Token
from Player import *
import copy
import random

class AggressivePlayer(Player):

    def analyzeFirstThrow(self, card, dice):
        analyzedDice = copy.deepcopy(dice)
        analyzedDice = self.rerollBranches(analyzedDice)
        if self.cardCanBeTackled(card):
            # Reroll more damage/range + more Jaeger if card is tackled
            analyzedDice = self.rerollExtraDamage(card, analyzedDice)
        else:
            analyzedDice = self.rerollForJaeger(analyzedDice)
        return analyzedDice


# Tokens
# Scored cards
# Player personality
# Ramapaa
# pohtija x 2
# safe player
