import copy
from Card import Card
from random import shuffle

defaultCards = [Card(name='Mäyrä',damage=1,range=0,points=3),
                Card(name='Mäyrä',damage=1,range=0,points=3),
                Card(name='Mäyrä',damage=1,range=0,points=3),
                Card(name='Fasaani',damage=1,range=1,points=5),
                Card(name='Fasaani',damage=1,range=1,points=5),
                Card(name='Fasaani',damage=1,range=1,points=5),
                Card(name='Fasaani',damage=1,range=2,points=7),
                Card(name='Fasaani',damage=1,range=2,points=7),
                Card(name='Fasaani',damage=1,range=3,points=9),
                Card(name='Peura',damage=1,range=1,points=6),
                Card(name='Peura',damage=1,range=1,points=6),
                Card(name='Peura',damage=1,range=2,points=8),
                Card(name='Peura',damage=1,range=2,points=8),
                Card(name='Peura',damage=1,range=2,points=8),
                Card(name='Peura',damage=1,range=3,points=10),
                Card(name='Saksanhirvi',damage=2,range=1,points=9),
                Card(name='Saksanhirvi',damage=2,range=1,points=9),
                Card(name='Saksanhirvi',damage=2,range=1,points=9),
                Card(name='Saksanhirvi',damage=2,range=2,points=15),
                Card(name='Saksanhirvi',damage=2,range=2,points=15),
                Card(name='Saksanhirvi',damage=2,range=3,points=20),
                Card(name='Karhu',damage=4,range=0,points=15),
                Card(name='Karhu',damage=4,range=1,points=20)
                ]

customCards =  [Card(name='Mäyrä',damage=1,range=0,points=2),
                Card(name='Mäyrä',damage=1,range=0,points=2),
                Card(name='Mäyrä',damage=1,range=0,points=2),
                Card(name='Fasaani',damage=1,range=1,points=3),
                Card(name='Fasaani',damage=1,range=1,points=3),
                Card(name='Fasaani',damage=1,range=1,points=3),
                Card(name='Fasaani',damage=1,range=2,points=5),
                Card(name='Fasaani',damage=1,range=2,points=5),
                Card(name='Fasaani',damage=1,range=3,points=6),
                Card(name='Peura',damage=1,range=1,points=6),
                Card(name='Peura',damage=1,range=1,points=6),
                Card(name='Peura',damage=1,range=2,points=7),
                Card(name='Peura',damage=1,range=2,points=7),
                Card(name='Peura',damage=1,range=2,points=7),
                Card(name='Peura',damage=1,range=3,points=9),
                Card(name='Saksanhirvi',damage=2,range=1,points=9),
                Card(name='Saksanhirvi',damage=2,range=1,points=9),
                Card(name='Saksanhirvi',damage=2,range=1,points=9),
                Card(name='Saksanhirvi',damage=2,range=2,points=12),
                Card(name='Saksanhirvi',damage=2,range=2,points=12),
                Card(name='Saksanhirvi',damage=2,range=3,points=12),
                Card(name='Karhu',damage=4,range=0,points=12),
                Card(name='Karhu',damage=4,range=1,points=15)
                ]

kallesCards =  [Card(name='Mäyrä',damage=1,range=0,points=2),
                Card(name='Mäyrä',damage=1,range=0,points=2),
                Card(name='Mäyrä',damage=1,range=0,points=2),
                Card(name='Fasaani',damage=1,range=1,points=3),
                Card(name='Fasaani',damage=1,range=1,points=3),
                Card(name='Fasaani',damage=1,range=1,points=3),
                Card(name='Fasaani',damage=1,range=2,points=5),
                Card(name='Fasaani',damage=1,range=2,points=5),
                Card(name='Fasaani',damage=1,range=3,points=6),
                Card(name='Peura',damage=1,range=1,points=6),
                Card(name='Peura',damage=1,range=1,points=6),
                Card(name='Peura',damage=1,range=2,points=7),
                Card(name='Peura',damage=1,range=2,points=7),
                Card(name='Peura',damage=1,range=2,points=7),
                Card(name='Peura',damage=1,range=3,points=9),
                Card(name='Saksanhirvi',damage=2,range=1,points=9),
                Card(name='Saksanhirvi',damage=2,range=1,points=9),
                Card(name='Saksanhirvi',damage=2,range=1,points=9),
                Card(name='Saksanhirvi',damage=2,range=2,points=13),
                Card(name='Saksanhirvi',damage=2,range=2,points=13),
                Card(name='Saksanhirvi',damage=2,range=3,points=17),
                Card(name='Karhu',damage=4,range=0,points=13),
                Card(name='Karhu',damage=4,range=1,points=17)
                ]

class Deck():
    def __init__(self, cards=None, defaultDeck=False):
        if defaultDeck:
            defaultDeckCopy = copy.deepcopy(customCards)
            shuffle(defaultDeckCopy)
            self.cardPile = defaultDeckCopy
        else:
            self.cardPile = cards if (cards != None and len(cards) > 0) else []
    def numCardsInDeck(self):
        return len(self.cardPile)
    def addCard(self, animalCard):
        self.cardPile.append(animalCard)
    def popCard(self):
        if len(self.cardPile) > 0:
            return self.cardPile.pop()
        else:
            return None
