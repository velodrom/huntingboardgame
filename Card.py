allNames  = ['Karhu', 'Hirvi', 'Mayra', 'Fasaani']
allDamage = [0, 1, 2, 3, 4]
allRange  = [0, 1, 2, 3, 4]
allPoints = [2, 5, 7, 15, 20]

class Card():
    def __init__(self, name='Karhu', damage=2, range=2, points=15):
        self.name = name
        self.damage = damage
        self.range   = range
        self.points = points
    def getName(self):
        return self.name
    def getDamage(self):
        return self.damage
    def getRange(self):
        return self.range
    def getPoints(self):
        return self.points