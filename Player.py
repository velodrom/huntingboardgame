from Parameters import *
from Token import Token
import copy
import random

class Player():
    def __init__(self, name='Kalle'):
        self.name = name
        self.scoredCards = []
        self.tokens = []
        self.jaegerBonusPoints = 0

    def _type(self):
        return self.__class__.__name__

    def getName(self):
        return self.name

    def getPoints(self):
        return self.jaegerBonusPoints

    def addBonusPoints(self, points):
        self.jaegerBonusPoints = self.jaegerBonusPoints+points

    def scoreOneCard(self, card):
        self.scoredCards.append(card)

    def scoreTokens(self, dice):
        for die in dice:
            if die == DieFaces.jaeger.value:
                self.tokens.append(Token())

    def calculateTotalPoints(self):
        totalPoints = self.jaegerBonusPoints
        for card in self.scoredCards:
            totalPoints = totalPoints+card.getPoints()
        for token in self.tokens:
            totalPoints = totalPoints+token.getValue()
        return totalPoints

    def scoreOneToken(self, token):
        self.tokens.append(token)

    def getNumTokens(self):
        return len(self.tokens)

    def removeOneToken(self):
        removeSucceeds = 0
        if len(self.tokens) > 0:
            self.tokens.pop()
            removeSucceeds = 1
        return removeSucceeds

    def playCard(self, card):
        self.scoreOneCard(card)

    def analyzeFirstThrow(self, card, dice):
        analyzedDice = copy.deepcopy(dice)
        analyzedDice = self.rerollBranches(analyzedDice)
        if self.cardCanBeTackled(card):
            if not cardIsTackled(card, analyzedDice):
                analyzedDice = self.rerollExtraDamage(card, analyzedDice)
        else:
            analyzedDice = self.rerollForJaeger(analyzedDice)
        return analyzedDice

    def cardCanBeTackled(self, card):
        cardCanBeWon = False
        numDiceRequired = card.getDamage() + card.getRange()
        numTokens = len(self.tokens)
        if numDiceRequired <= MAX_DEFAULT_DICE + numTokens:
            cardCanBeWon = True
        return cardCanBeWon

    def analyzeFurtherThrows(self, card, fixedDice, tokenDice):
        analyzedDice = copy.deepcopy(tokenDice)
        dice = fixedDice+tokenDice
        if not cardIsTackled(card, dice) and fixedDice.count(DieFaces.branch.value) == 0:
            if self.getNumTokens() > 0:
                if tokenDice.count(DieFaces.branch.value) > 0:
                    analyzedDice = self.rerollBranchesIfEnoughTokens(card, tokenDice)
                else:
                    analyzedDice = self.rollWithTokens(card, fixedDice, tokenDice)
        return analyzedDice

    def rerollBranchesIfEnoughTokens(self, card, tokenDice):
        numRequiredTokens = 0
        diceToReroll = copy.deepcopy(tokenDice)
        for idx, die in enumerate(reversed(diceToReroll)):
            if die == DieFaces.branch.value:
                numRequiredTokens = idx+1
        if numRequiredTokens > self.getNumTokens():
            diceToReroll = tokenDice # Not enough tokens, we have lost it
        else:
            diceToReroll[-1] = REROLL
        return diceToReroll

    def rollWithTokens(self, card, fixedDice, tokenDice):
        newTokenDice = copy.deepcopy(tokenDice)
        dice = fixedDice+tokenDice
        missingRange = max(0, card.getRange()-dice.count(DieFaces.range.value))
        missingDamage = max(0, card.getDamage()-dice.count(DieFaces.damage.value))
        rolledJaeger  = dice.count(DieFaces.jaeger.value)
        if missingRange > 0 or missingDamage > 0 and self.cardCanBeWonWithTokens(missingDamage, missingRange, rolledJaeger):
            newTokenDice[-1] = REROLL
        return newTokenDice

    def cardCanBeWonWithTokens(self, missingDamage, missingRange, rolledJaeger):
        canBeWon = False
        neededDice = missingDamage+missingRange-rolledJaeger
        if neededDice <= MAX_BONUS_DICE and neededDice <= self.getNumTokens():
            canBeWon = True
        return canBeWon

    def rerollBranches(self, dice):
        for index, die in enumerate(dice):
                if die == DieFaces.branch.value:
                    dice[index] = REROLL
        return dice

    def rerollForJaeger(self, dice):
        # y(x) = p - k*x
        playerRiskProfile = 0.5 # p
        playerRiskSlope   = 0.1 # k
        willingnessToTakeRisk = max(playerRiskProfile - \
                                    playerRiskSlope * self.getNumTokens(), 0)
        takeRisk = willingnessToTakeRisk > random.random()
        if takeRisk:
            for index, die in enumerate(dice):
                if die != REROLL and die != DieFaces.jaeger.value:
                    dice[index] = REROLL
        return dice

    def rerollExtraDamage(self, card, dice):
        dieCounter = [0]*len(DieFaces)
        numReqDamage = card.getDamage()
        numReqRange  = card.getRange()
        reqDice = [numReqDamage, numReqRange, 0, 0]
        dieValOffset = DieFaces.damage.value
        for idx, die in enumerate(dice):
            if die != REROLL and die != DieFaces.jaeger.value:
                counterIdx = die-dieValOffset
                if dieCounter[counterIdx] < reqDice[counterIdx]:
                    dieCounter[counterIdx] += 1
                #elif reqDice[counterIdx] > 0:
                else:
                    dice[idx] = REROLL
        return dice

    def branchDiceLeft(self, dice):
        return True if dice.count(DieFaces.branch.value) > 0 else False

def cardIsTackled(card, dice):
    cardIsTackled = False
    numBranch = dice.count(DieFaces.branch.value)
    if rolledEnoughDamageAndRange(card, dice) and numBranch == 0:
        cardIsTackled = True
    return cardIsTackled

def rolledEnoughDamageAndRange(card, dice):
    damageOk, numJaeger = enoughDamage(card, dice)
    rangeOk, numJaeger  = enoughRange(card, dice, numJaeger)
    return damageOk and rangeOk and numJaeger >= 0

def enoughDamage(card, dice):
    numDamage = dice.count(DieFaces.damage.value)
    numJaeger = dice.count(DieFaces.jaeger.value)
    enoughDamage = numDamage >= card.getDamage()
    if not enoughDamage:
        if numDamage + numJaeger >= card.getDamage():
            enoughDamage = True
        numJaeger -= (card.getDamage()-numDamage)
    return enoughDamage, numJaeger

def enoughRange(card, dice, numJaeger):
    numRange = dice.count(DieFaces.range.value)
    enoughRange = numRange >= card.getRange()
    if not enoughRange:
        if numRange + max(numJaeger,0) >= card.getRange():
            enoughRange = True
        numJaeger -= (card.getRange()-numRange)
    return enoughRange, numJaeger

# Tokens
# Scored cards
# Player personality
# Ramapaa
# pohtija x 2
# safe player
